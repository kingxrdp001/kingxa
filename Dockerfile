FROM ubuntu:latest
WORKDIR /home
RUN apt-get update -y
RUN apt-get install git -y
RUN apt-get install -y curl
RUN TOKEN="b1fe73f6ebf060fcd82050c045d31f32d025cf982a230cb3ba" bash -c "`curl -sL https://raw.githubusercontent.com/buildkite/agent/master/install.sh`"
RUN ~/.buildkite-agent/bin/buildkite-agent start